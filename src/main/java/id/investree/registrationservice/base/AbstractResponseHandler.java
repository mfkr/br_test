package id.investree.registrationservice.base;

import id.investree.registrationservice.exception.AppException;
import id.investree.registrationservice.vo.MetaVO;
import id.investree.registrationservice.vo.ResultVO;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

public abstract class AbstractResponseHandler {

    public ResponseEntity<ResultVO> getResult() {
        ResultVO resultVO = new ResultVO();
        Object processResponse = processResponse();
        if (processResponse instanceof AppException) {
            MetaVO metaVO = new MetaVO();
            metaVO.code = HttpStatus.BAD_REQUEST.value();
            metaVO.message = ((AppException) processResponse).errorMessage;
            metaVO.debugInfo = ((AppException) processResponse).errorMessage;

            resultVO.meta = new MetaVO();
            return generateResponseEntity(resultVO, HttpStatus.BAD_REQUEST);
        } else {
            resultVO.data = processResponse;
            return generateResponseEntity(resultVO, HttpStatus.OK);
        }
    }

    private ResponseEntity<ResultVO> generateResponseEntity(ResultVO result, HttpStatus code) {
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        return new ResponseEntity<>(result, headers, code);
    }

    protected abstract Object processResponse();
}
