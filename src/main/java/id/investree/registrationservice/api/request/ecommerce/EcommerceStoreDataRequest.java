package id.investree.registrationservice.api.request.ecommerce;

public class EcommerceStoreDataRequest {

    public String labaUsaha;
    public String nilaiRataPenjualan;
    public String offlineStoreAddress;
    public String offlineStoreProvince;
    public String offlineStoreKabKot;
    public String offlineStoreKecamatan;
    public String offlineStoreKelurahan;
    public String offlineStorePostalCode;
}