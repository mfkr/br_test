package id.investree.registrationservice.api.request.borrower;

public class BorrowerDataRequest {

    public Long registrationId;
    public String vaNumber;
    public String fullName;
    public String gender;
    public String salutation;
    public String emailAddress;
    public String companyProvince;
    public String companyKabKot;
    public String mobilePrefix;
    public String mobileNumber;
    public Integer companyType;
    public String companyName;
    public String ipAddress;
    public Integer borrowerStatus;
    public Integer referralSchema;

}
