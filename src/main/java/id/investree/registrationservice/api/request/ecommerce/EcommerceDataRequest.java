package id.investree.registrationservice.api.request.ecommerce;

public class EcommerceDataRequest {

    public Long registrationId;
    public String emailAddress;
    public String mobileNumber;
    public String fullName;
}
