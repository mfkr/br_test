package id.investree.registrationservice.api.request.consumer;

public class ConsumerDataRequest {

    public Long registrationId;
    public String vaNumber;
    public String number;
    public Integer partnershipId;
    public String fullName;
    public String gender;
    public String salutation;
    public String emailAddress;
    public String mobilePrefix;
    public String mobileNumber;
    public String ipAddress;
}
