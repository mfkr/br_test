package id.investree.registrationservice.api.request.borrower;

public class BorrowerCompanyProfileRequest {

    public String salutation;
    public String fullName;
    public String emailAddress;
    public String mobileNumber;
    public String loanType;
    public String companyPosition;
    public String companyType;
    public String companyName;
    public String companyIndustryCategoryId;
    public String companyIndustryTypeId;
    public String companyDesc;
    public String numberOfEmployee;
    public String companyYearStart;
    public String companyAddress;
    public String companyProvince;
    public String companyKabKot;
    public String companyKecamatan;
    public String companyKelurahan;
    public String companyPostalCode;
    public String companyPhone;
    public String companyFax;
    public String cdocSiupNumber;
    //    public MultipartFile cdocSiupFile;
    public String companyBankNameId;
    public String companyBankAccountName;
    public String companyBankNumber;
    public String number;
    public String ipAddress;
}
