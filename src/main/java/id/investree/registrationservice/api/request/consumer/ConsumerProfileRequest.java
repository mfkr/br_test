package id.investree.registrationservice.api.request.consumer;

import java.util.Date;

public class ConsumerProfileRequest {

    public String salutation;
    public String fullName;
    public String initial;
    public String pob;
    public String dob;
    public String ktp;
    public String ktpExpired;
    public String npwp;
    public String educational;
    public String marriageStatus;
    public String relationName;
    public Integer amountOfDependent;
    public String mobileNumber;
    public String phoneNumber;
    public String ktpAddress;
    public String ktpProvince;
    public String ktpKabKot;
    public String ktpKecamatan;
    public String ktpKelurahan;
    public String ktpPostalCode;
    public String domSameKtp;
    public String domAddress;
    public String domProvince;
    public String domKabKot;
    public String domKecamatan;
    public String domKelurahan;
    public String domPostalCode;
    public String kerabatName;
    public String kerabatRelation;
    public String kerabatPhone;
    public String kerabatHp;
    public String kerabatAddress;
    public String kerabatProvince;
    public String kerabatKabKot;
    public String kerabatKecamatan;
    public String kerabatKelurahan;
    public String kerabatPostalCode;
    public String ipAddress;
    public String rightData;
    public String fillFinishDate;
    public Date latestUpdated;
    public String nik;
    public Integer bankNameId;
    public String bankAccountName;
    public String bankAccountNo;
    public Date createdDate;

}
