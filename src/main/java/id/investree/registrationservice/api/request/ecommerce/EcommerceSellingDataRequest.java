package id.investree.registrationservice.api.request.ecommerce;

public class EcommerceSellingDataRequest {

    public String lamaBerjualan;
    public String lamaBerjualanOnline;
    public String nilaiRataPenjualan;
    public Integer companyType;
    public String companyName;
    public Integer eplId;
    public String linkToko;
    public String sellerLocation;
}