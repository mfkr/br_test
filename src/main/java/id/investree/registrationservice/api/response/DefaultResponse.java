package id.investree.registrationservice.api.response;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DefaultResponse {

    public Long registrationId;
    public Long borrowerId;
    public String message;
}
