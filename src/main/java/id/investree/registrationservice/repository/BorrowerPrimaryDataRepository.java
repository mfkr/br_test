package id.investree.registrationservice.repository;

import id.investree.registrationservice.entity.BorrowerPrimaryData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BorrowerPrimaryDataRepository extends JpaRepository<BorrowerPrimaryData, Long> {

    BorrowerPrimaryData findByRegistrationId(Long registrationId);
}
