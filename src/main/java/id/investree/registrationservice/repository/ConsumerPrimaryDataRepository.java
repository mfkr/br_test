package id.investree.registrationservice.repository;

import id.investree.registrationservice.entity.ConsumerPrimaryData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConsumerPrimaryDataRepository extends JpaRepository<ConsumerPrimaryData, Long> {

    ConsumerPrimaryData findByRegistrationId(Long registrationId);
}
