package id.investree.registrationservice.repository;

import id.investree.registrationservice.entity.EcommercePrimaryData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EcommercePrimaryDataRepository extends JpaRepository<EcommercePrimaryData, Long> {

    EcommercePrimaryData findByBorrowerId(Long borrowerId);
}
