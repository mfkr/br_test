package id.investree.registrationservice.constant;

public enum StatusCode {
    OK,
    ERROR,
    NOT_FOUND
}