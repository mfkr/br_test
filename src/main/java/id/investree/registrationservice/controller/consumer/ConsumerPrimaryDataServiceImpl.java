package id.investree.registrationservice.controller.consumer;

import id.investree.registrationservice.api.request.consumer.ConsumerDataRequest;
import id.investree.registrationservice.api.request.consumer.ConsumerProfileRequest;
import id.investree.registrationservice.entity.ConsumerPrimaryData;
import id.investree.registrationservice.exception.DataNotFoundException;
import id.investree.registrationservice.repository.ConsumerPrimaryDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class ConsumerPrimaryDataServiceImpl implements ConsumerPrimaryDataService {

    @Autowired
    private ConsumerPrimaryDataRepository consumerRepository;

    @Override
    public ConsumerPrimaryData insert(ConsumerDataRequest request) {
        ConsumerPrimaryData data = new ConsumerPrimaryData();
        data.registrationId = request.registrationId;
        data.number = request.number;
        data.vaNumber = request.vaNumber;
        data.partnershipId = request.partnershipId;
        data.fullName = request.fullName;
        data.gender = request.gender;
        data.salutation = request.salutation;
        data.emailAddress = request.emailAddress;
        data.mobilePrefix = request.mobilePrefix;
        data.mobileNumber = request.mobileNumber;
        data.ipAddress = request.ipAddress;
        data.activationDate = new Date();
        data.lastLogin = new Date();
        data.createdDate = new Date();

        consumerRepository.save(data);
        return data;
    }

    @Override
    public ConsumerPrimaryData updateProfile(Long registrationId, ConsumerProfileRequest request) {
        ConsumerPrimaryData data = consumerRepository.findByRegistrationId(registrationId);
        if (null == data) throw new DataNotFoundException();

        data.salutation = (null == request.salutation) ? data.salutation : request.salutation;
        data.fullName = (null == request.fullName) ? data.fullName : request.fullName;
        data.initial = (null == request.initial) ? data.initial : request.initial;
        data.pob = (null == request.pob) ? data.pob : request.pob;
        data.dob = (null == request.dob) ? data.dob : request.dob;
        data.ktp = (null == request.ktp) ? data.ktp : request.ktp;
        data.ktpExpired = (null == request.ktpExpired) ? data.ktpExpired : request.ktpExpired;
        data.npwp = (null == request.npwp) ? data.npwp : request.npwp;
        data.educational = (null == request.educational) ? data.educational : request.educational;
        data.marriageStatus = (null == request.marriageStatus) ? data.marriageStatus : request.marriageStatus;
        data.relationName = (null == request.relationName) ? data.relationName : request.relationName;
        data.amountOfDependent = (null == request.amountOfDependent) ? data.amountOfDependent : request.amountOfDependent;
        data.mobileNumber = (null == request.mobileNumber) ? data.mobileNumber : request.mobileNumber;
        data.phoneNumber = (null == request.phoneNumber) ? data.phoneNumber : request.phoneNumber;
        data.ktpAddress = (null == request.ktpAddress) ? data.ktpAddress : request.ktpAddress;
        data.ktpProvince = (null == request.ktpProvince) ? data.ktpProvince : request.ktpProvince;
        data.ktpKabKot = (null == request.ktpKabKot) ? data.ktpKabKot : request.ktpKabKot;
        data.ktpKecamatan = (null == request.ktpKecamatan) ? data.ktpKecamatan : request.ktpKecamatan;
        data.ktpKelurahan = (null == request.ktpKelurahan) ? data.ktpKelurahan : request.ktpKelurahan;
        data.ktpPostalCode = (null == request.ktpPostalCode) ? data.ktpPostalCode : request.ktpPostalCode;
        data.domSameKtp = (null == request.domSameKtp) ? data.domSameKtp : request.domSameKtp;
        data.domAddress = (null == request.domAddress) ? data.domAddress : request.domAddress;
        data.domProvince = (null == request.domProvince) ? data.domProvince : request.domProvince;
        data.domKabKot = (null == request.domKabKot) ? data.domKabKot : request.domKabKot;
        data.domKecamatan = (null == request.domKecamatan) ? data.domKecamatan : request.domKecamatan;
        data.domKelurahan = (null == request.domKelurahan) ? data.domKelurahan : request.domKelurahan;
        data.domPostalCode = (null == request.domPostalCode) ? data.domPostalCode : request.domPostalCode;
        data.kerabatName = (null == request.kerabatName) ? data.kerabatName : request.kerabatName;
        data.kerabatRelation = (null == request.kerabatRelation) ? data.kerabatRelation : request.kerabatRelation;
        data.kerabatPhone = (null == request.kerabatPhone) ? data.kerabatPhone : request.kerabatPhone;
        data.kerabatHp = (null == request.kerabatHp) ? data.kerabatHp : request.kerabatHp;
        data.kerabatAddress = (null == request.kerabatAddress) ? data.kerabatAddress : request.kerabatAddress;
        data.kerabatProvince = (null == request.kerabatProvince) ? data.kerabatProvince : request.kerabatProvince;
        data.kerabatKabKot = (null == request.kerabatKabKot) ? data.kerabatKabKot : request.kerabatKabKot;
        data.kerabatKecamatan = (null == request.kerabatKecamatan) ? data.kerabatKecamatan : request.kerabatKecamatan;
        data.kerabatKelurahan = (null == request.kerabatKelurahan) ? data.kerabatKelurahan : request.kerabatKelurahan;
        data.kerabatPostalCode = (null == request.kerabatPostalCode) ? data.kerabatPostalCode : request.kerabatPostalCode;
        data.ipAddress = (null == request.ipAddress) ? data.ipAddress : request.ipAddress;
        data.rightData = (null == request.rightData) ? data.rightData : request.rightData;
        data.fillFinishDate = (null == request.fillFinishDate) ? data.fillFinishDate : request.fillFinishDate;
        data.latestUpdated = (null == request.latestUpdated) ? data.latestUpdated : request.latestUpdated;
        data.nik = (null == request.nik) ? data.nik : request.nik;
        data.bankNameId = (null == request.bankNameId) ? data.bankNameId : request.bankNameId;
        data.bankAccountName = (null == request.bankAccountName) ? data.bankAccountName : request.bankAccountName;
        data.bankAccountNo = (null == request.bankAccountNo) ? data.bankAccountNo : request.bankAccountNo;
        data.createdDate = (null == request.createdDate) ? data.createdDate : request.createdDate;

        consumerRepository.save(data);
        return data;
    }

    @Override
    public ConsumerPrimaryData getConsumer(Long registrationId) {
        ConsumerPrimaryData data = consumerRepository.findByRegistrationId(registrationId);
        if (null == data) throw new DataNotFoundException();
        return data;
    }
}