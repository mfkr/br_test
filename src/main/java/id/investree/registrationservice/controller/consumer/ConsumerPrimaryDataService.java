package id.investree.registrationservice.controller.consumer;

import id.investree.registrationservice.api.request.consumer.ConsumerDataRequest;
import id.investree.registrationservice.api.request.consumer.ConsumerProfileRequest;
import id.investree.registrationservice.entity.ConsumerPrimaryData;

public interface ConsumerPrimaryDataService {

    ConsumerPrimaryData insert(ConsumerDataRequest request);

    ConsumerPrimaryData updateProfile(Long registrationId, ConsumerProfileRequest request);

    ConsumerPrimaryData getConsumer(Long registrationId);
}
