package id.investree.registrationservice.controller.ecommerce;

import id.investree.registrationservice.api.request.ecommerce.EcommerceDataRequest;
import id.investree.registrationservice.api.request.ecommerce.EcommerceSellingDataRequest;
import id.investree.registrationservice.api.request.ecommerce.EcommerceStoreDataRequest;
import id.investree.registrationservice.api.response.DefaultResponse;
import id.investree.registrationservice.base.BaseController;
import id.investree.registrationservice.entity.EcommercePrimaryData;
import id.investree.registrationservice.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/registration/ecommerce", produces = MediaType.APPLICATION_JSON_VALUE)
public class EcommercePrimaryDataController extends BaseController {

    @Autowired
    private EcommercePrimaryDataService service;

    @Autowired
    private MessageSource messageSource;

    @PostMapping
    public ResponseEntity<ResultVO> insert(@RequestBody EcommerceDataRequest request) {
        EcommercePrimaryData data = service.insert(request);

        DefaultResponse response = new DefaultResponse();
        response.borrowerId = data.borrowerId;
        response.message = messageSource.getMessage("insert.success", null, LocaleContextHolder.getLocale());
        return abstractResponseHandler(response).getResult();
    }

    @PutMapping("/selling/{borrowerId}")
    public ResponseEntity<ResultVO> updateSellingData(@PathVariable Long borrowerId, @RequestBody EcommerceSellingDataRequest request) {
        EcommercePrimaryData data = service.updateSellingData(borrowerId, request);

        DefaultResponse response = new DefaultResponse();
        response.borrowerId = data.borrowerId;
        response.message = messageSource.getMessage("update.success", null, LocaleContextHolder.getLocale());
        return abstractResponseHandler(response).getResult();
    }

    @PutMapping("/store/{borrowerId}")
    public ResponseEntity<ResultVO> updateStoreData(@PathVariable Long borrowerId, @RequestBody EcommerceStoreDataRequest request) {
        EcommercePrimaryData data = service.updateStoreData(borrowerId, request);

        DefaultResponse response = new DefaultResponse();
        response.borrowerId = data.borrowerId;
        response.message = messageSource.getMessage("update.success", null, LocaleContextHolder.getLocale());
        return abstractResponseHandler(response).getResult();
    }
}
