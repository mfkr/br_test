package id.investree.registrationservice.controller.ecommerce;

import id.investree.registrationservice.api.request.ecommerce.EcommerceDataRequest;
import id.investree.registrationservice.api.request.ecommerce.EcommerceSellingDataRequest;
import id.investree.registrationservice.api.request.ecommerce.EcommerceStoreDataRequest;
import id.investree.registrationservice.constant.StatusCode;
import id.investree.registrationservice.entity.BorrowerPrimaryData;
import id.investree.registrationservice.entity.EcommercePrimaryData;
import id.investree.registrationservice.exception.AppException;
import id.investree.registrationservice.exception.DataNotFoundException;
import id.investree.registrationservice.repository.BorrowerPrimaryDataRepository;
import id.investree.registrationservice.repository.EcommercePrimaryDataRepository;
import id.investree.registrationservice.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class EcommercePrimaryDataServiceImpl implements EcommercePrimaryDataService {

    @Autowired
    private BorrowerPrimaryDataRepository borrowerRepository;

    @Autowired
    private EcommercePrimaryDataRepository ecommerceRepository;

    @Autowired
    private MessageSource messageSource;

    @Override
    public EcommercePrimaryData insert(EcommerceDataRequest request) {
        BorrowerPrimaryData borrowerData = new BorrowerPrimaryData();
        borrowerData.registrationId = request.registrationId;
        borrowerData.emailAddress = request.emailAddress;
        borrowerData.mobileNumber = request.mobileNumber;
        borrowerData.fullName = request.fullName;
        BorrowerPrimaryData savedBorrowerData = borrowerRepository.save(borrowerData);

        if (null == savedBorrowerData) throw new AppException(
                messageSource.getMessage("insert.failed", null, LocaleContextHolder.getLocale()),
                StatusCode.ERROR, HttpStatus.BAD_REQUEST);

        EcommercePrimaryData ecommerceData = new EcommercePrimaryData();
        ecommerceData.borrowerId = savedBorrowerData.id;
        ecommerceData.createdAt = new Date();
        ecommerceData.updatedAt = new Date();
        ecommerceData.personalData = "Y";

        ecommerceRepository.save(ecommerceData);
        return ecommerceData;
    }

    @Override
    public EcommercePrimaryData updateSellingData(Long borrowerId, EcommerceSellingDataRequest request) {
        EcommercePrimaryData data = ecommerceRepository.findByBorrowerId(borrowerId);
        if (null == data) throw new DataNotFoundException();

        data.lamaBerjualan = DateUtils.toDate(request.lamaBerjualan);
        data.lamaBerjualanOnline = request.lamaBerjualanOnline;
        data.nilaiRataPenjualan = request.nilaiRataPenjualan;
        data.companyType = request.companyType;
        data.companyName = request.companyName;
        data.eplId = request.eplId;
        data.linkToko = request.linkToko;
        data.sellerLocation = request.sellerLocation;

        ecommerceRepository.save(data);
        return data;
    }

    @Override
    public EcommercePrimaryData updateStoreData(Long borrowerId, EcommerceStoreDataRequest request) {
        EcommercePrimaryData data = ecommerceRepository.findByBorrowerId(borrowerId);
        if (null == data) throw new DataNotFoundException();

        data.labaUsaha = request.labaUsaha;
        data.nilaiRataPenjualan = request.nilaiRataPenjualan;
        data.offlineStoreAddress = request.offlineStoreAddress;
        data.offlineStoreProvince = request.offlineStoreProvince;
        data.offlineStoreKabKot = request.offlineStoreKabKot;
        data.offlineStoreKecamatan = request.offlineStoreKecamatan;
        data.offlineStoreKelurahan = request.offlineStoreKelurahan;
        data.offlineStorePostalCode = request.offlineStorePostalCode;

        ecommerceRepository.save(data);
        return data;
    }
}