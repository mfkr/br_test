package id.investree.registrationservice.controller.ecommerce;

import id.investree.registrationservice.api.request.ecommerce.EcommerceDataRequest;
import id.investree.registrationservice.api.request.ecommerce.EcommerceSellingDataRequest;
import id.investree.registrationservice.api.request.ecommerce.EcommerceStoreDataRequest;
import id.investree.registrationservice.entity.EcommercePrimaryData;

public interface EcommercePrimaryDataService {

    EcommercePrimaryData insert(EcommerceDataRequest request);

    EcommercePrimaryData updateSellingData(Long borrowerId, EcommerceSellingDataRequest request);

    EcommercePrimaryData updateStoreData(Long borrowerId, EcommerceStoreDataRequest request);

}
