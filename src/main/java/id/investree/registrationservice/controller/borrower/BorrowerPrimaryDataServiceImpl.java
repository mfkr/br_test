package id.investree.registrationservice.controller.borrower;

import id.investree.registrationservice.api.request.borrower.BorrowerCompanyProfileRequest;
import id.investree.registrationservice.api.request.borrower.BorrowerDataRequest;
import id.investree.registrationservice.entity.BorrowerPrimaryData;
import id.investree.registrationservice.exception.DataNotFoundException;
import id.investree.registrationservice.repository.BorrowerPrimaryDataRepository;
import id.investree.registrationservice.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class BorrowerPrimaryDataServiceImpl implements BorrowerPrimaryDataService {

    @Autowired
    private BorrowerPrimaryDataRepository borrowerRepository;

    @Override
    public BorrowerPrimaryData insert(BorrowerDataRequest request) {
        BorrowerPrimaryData data = new BorrowerPrimaryData();
        data.registrationId = request.registrationId;
        data.vaNumber = request.vaNumber;
        data.fullName = request.fullName;
        data.gender = request.gender;
        data.salutation = request.salutation;
        data.emailAddress = request.emailAddress;
        data.companyProvince = request.companyProvince;
        data.companyKabKot = request.companyKabKot;
        data.mobilePrefix = request.mobilePrefix;
        data.mobileNumber = request.mobileNumber;
        data.companyType = request.companyType;
        data.companyName = request.companyName;
        data.activationDate = new Date();
        data.lastLogin = new Date();
        data.createdDate = new Date();
        data.ipAddress = request.ipAddress;
        data.borrowerStatus = request.borrowerStatus;
        data.referralSchema = request.referralSchema;

        borrowerRepository.save(data);
        return data;
    }

    @Override
    public BorrowerPrimaryData updateCompanyProfile(Long registrationId, BorrowerCompanyProfileRequest request) {
        BorrowerPrimaryData data = borrowerRepository.findByRegistrationId(registrationId);
        if (null == data) throw new DataNotFoundException();

        data.salutation = request.salutation;
        data.fullName = request.fullName;
        data.emailAddress = request.emailAddress;
        data.mobileNumber = request.mobileNumber;
        data.loanType = request.loanType;
        data.companyPosition = StringUtils.toInt(request.companyPosition);
        data.companyType = StringUtils.toInt(request.companyType);
        data.companyName = request.companyName;
        data.companyIndustryCategoryId = StringUtils.toInt(request.companyIndustryCategoryId);
        data.companyIndustryTypeId = StringUtils.toInt(request.companyIndustryTypeId);
        data.companyDesc = request.companyDesc;
        data.numberOfEmployee = StringUtils.toInt(request.numberOfEmployee);
        data.companyYearStart = StringUtils.toInt(request.companyYearStart);
        data.companyAddress = request.companyAddress;
        data.companyProvince = request.companyProvince;
        data.companyKabKot = request.companyKabKot;
        data.companyKecamatan = request.companyKecamatan;
        data.companyKelurahan = request.companyKelurahan;
        data.companyPostalCode = request.companyPostalCode;
        data.companyPhone = request.companyPhone;
        data.companyFax = request.companyFax;
        data.cdocSiupNumber = request.cdocSiupNumber;

        // TODO: Multipart not implemented yet
        // data.cdocSiupFile = request.cdocSiupFile;

        data.companyBankNameId = StringUtils.toInt(request.companyBankNameId);
        data.companyBankAccountName = request.companyBankAccountName;
        data.companyBankNumber = request.companyBankNumber;
        data.latestUpdated = new Date();
        data.fillFinishDate = new Date();
        data.rightData = "Y";
        data.completeDocs = "Y";
        data.number = request.number;
        data.ipAddress = request.ipAddress;

        borrowerRepository.save(data);
        return data;
    }

    @Override
    public BorrowerPrimaryData getBorrowerData(Long registrationId) {
        BorrowerPrimaryData data = borrowerRepository.findByRegistrationId(registrationId);
        if (null == data) throw new DataNotFoundException();
        return data;
    }
}