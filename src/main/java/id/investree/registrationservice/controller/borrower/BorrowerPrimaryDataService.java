package id.investree.registrationservice.controller.borrower;

import id.investree.registrationservice.api.request.borrower.BorrowerCompanyProfileRequest;
import id.investree.registrationservice.api.request.borrower.BorrowerDataRequest;
import id.investree.registrationservice.entity.BorrowerPrimaryData;

public interface BorrowerPrimaryDataService {

    BorrowerPrimaryData insert(BorrowerDataRequest request);

    BorrowerPrimaryData updateCompanyProfile(Long registrationId, BorrowerCompanyProfileRequest request);

    BorrowerPrimaryData getBorrowerData(Long registrationId);
}
