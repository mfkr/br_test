package id.investree.registrationservice.controller.borrower;

import id.investree.registrationservice.api.request.borrower.BorrowerCompanyProfileRequest;
import id.investree.registrationservice.api.request.borrower.BorrowerDataRequest;
import id.investree.registrationservice.api.response.DefaultResponse;
import id.investree.registrationservice.base.BaseController;
import id.investree.registrationservice.entity.BorrowerPrimaryData;
import id.investree.registrationservice.exception.DataNotFoundException;
import id.investree.registrationservice.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/registration/borrower", produces = MediaType.APPLICATION_JSON_VALUE)
public class BorrowerPrimaryDataController extends BaseController {

    @Autowired
    private BorrowerPrimaryDataService service;

    @Autowired
    private MessageSource messageSource;

    @PostMapping
    public ResponseEntity<ResultVO> insert(@RequestBody BorrowerDataRequest request) {
        BorrowerPrimaryData data = service.insert(request);

        DefaultResponse response = new DefaultResponse();
        response.registrationId = data.registrationId;
        response.message = messageSource.getMessage("insert.success", null, LocaleContextHolder.getLocale());
        return abstractResponseHandler(response).getResult();
    }

    @PutMapping("/{registrationId}")
    public ResponseEntity<ResultVO> updateCompany(@PathVariable Long registrationId, @RequestBody BorrowerCompanyProfileRequest request) {
        BorrowerPrimaryData data = service.updateCompanyProfile(registrationId, request);
        if (null == data) throw new DataNotFoundException();

        DefaultResponse response = new DefaultResponse();
        response.registrationId = data.registrationId;
        response.message = messageSource.getMessage("update.success", null, LocaleContextHolder.getLocale());
        return abstractResponseHandler(response).getResult();
    }

    @GetMapping("/{registrationId}")
    public ResponseEntity<ResultVO> getBorrower(@PathVariable Long registrationId) {
        BorrowerPrimaryData data = service.getBorrowerData(registrationId);
        return abstractResponseHandler(data).getResult();
    }
}
