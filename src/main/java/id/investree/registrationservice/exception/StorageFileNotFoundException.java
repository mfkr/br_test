package id.investree.registrationservice.exception;

public class StorageFileNotFoundException extends RuntimeException {

    public StorageFileNotFoundException() {
        super("File not found");
    }
}
