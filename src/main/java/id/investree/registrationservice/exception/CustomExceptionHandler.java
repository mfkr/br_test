package id.investree.registrationservice.exception;

import id.investree.registrationservice.vo.MetaVO;
import id.investree.registrationservice.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    @Autowired
    private MessageSource messageSource;

    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        MetaVO metaVO = new MetaVO();
        metaVO.code = status.value();
        metaVO.message = ex.getLocalizedMessage();

        ResultVO resultVO = new ResultVO();
        resultVO.meta = metaVO;
        return new ResponseEntity<>(resultVO, status);
    }

    @ExceptionHandler(AppException.class)
    public ResponseEntity<ResultVO> badRequest(AppException ex) {
        MetaVO metaVO = new MetaVO();
        metaVO.code = ex.code.value();
        metaVO.message = ex.getLocalizedMessage();

        ResultVO resultVO = new ResultVO();
        resultVO.meta = metaVO;
        return new ResponseEntity<>(resultVO, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ResultVO> generalError(Exception ex) {
        MetaVO metaVO = new MetaVO();
        metaVO.code = HttpStatus.INTERNAL_SERVER_ERROR.value();
        metaVO.message = messageSource.getMessage("general.error", null, LocaleContextHolder.getLocale());
        metaVO.debugInfo = ex.getLocalizedMessage();

        ResultVO resultVO = new ResultVO();
        resultVO.meta = metaVO;
        return new ResponseEntity<>(resultVO, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(UserExistException.class)
    public ResponseEntity<ResultVO> userExist(UserExistException ex) {
        MetaVO metaVO = new MetaVO();
        metaVO.code = HttpStatus.BAD_REQUEST.value();
        metaVO.message = ex.getLocalizedMessage();

        ResultVO resultVO = new ResultVO();
        resultVO.meta = metaVO;
        return new ResponseEntity<>(resultVO, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DataNotFoundException.class)
    public ResponseEntity<ResultVO> dataNotFound(DataNotFoundException ex) {
        MetaVO metaVO = new MetaVO();
        metaVO.code = HttpStatus.NOT_FOUND.value();
        metaVO.message = ex.getLocalizedMessage();

        ResultVO resultVO = new ResultVO();
        resultVO.meta = metaVO;
        return new ResponseEntity<>(resultVO, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity<ResultVO> fileNotFound(StorageFileNotFoundException ex) {
        MetaVO metaVO = new MetaVO();
        metaVO.code = HttpStatus.NOT_FOUND.value();
        metaVO.message = ex.getLocalizedMessage();

        ResultVO resultVO = new ResultVO();
        resultVO.meta = metaVO;
        return new ResponseEntity<>(resultVO, HttpStatus.NOT_FOUND);
    }
}
