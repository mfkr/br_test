package id.investree.registrationservice.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "ecommerce_primary_data")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EcommercePrimaryData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "epd_id")
    public Long id;

    @Column(name = "epd_bpd_id")
    public Long borrowerId;

    @Column(name = "epd_company_name")
    public String companyName;

    @Column(name = "epd_link_toko")
    public String linkToko;

    @Column(name = "epd_operational_vintage")
    public Integer operationalVintage;

    @Column(name = "epd_anchor_vintage")
    public String anchorVintage;

    @Column(name = "epd_max_amount")
    public String maxAmount;

    @Column(name = "epd_sales_data")
    public String salesData;

    @Column(name = "epd_laba_usaha")
    public String labaUsaha;

    @Column(name = "epd_initial_loan_amount")
    public String initialLoanAmount;

    @Column(name = "epd_initial_loan_tenor")
    public String initialLoanTenor;

    @Column(name = "epd_created_at")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date createdAt;

    @Column(name = "epd_updated_at")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date updatedAt;

    @Column(name = "epd_epl_id")
    public Integer eplId;

    @Column(name = "epd_personal_data")
    public String personalData = "D";

    @Column(name = "epd_status")
    public String status = "Y";

    @Column(name = "epd_seller_location")
    public String sellerLocation;

    @Column(name = "epd_seller_phone_number")
    public String sellerPhoneNumber;

    @Column(name = "epd_seller_rating")
    public String sellerRating;

    @Column(name = "epd_seller_rating_internal")
    public String sellerRatingInternal;

    @Column(name = "epd_smpc_id")
    public String smpcId;

    @Column(name = "epd_year_of_establishment")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date yearOfEstablishment;

    @Column(name = "epd_is_facility")
    public String isFacility = "N";

    @Column(name = "epd_tempat_usaha")
    public String tempatUsaha;

    @Column(name = "epd_company_type")
    public Integer companyType;

    @Column(name = "epd_lama_berjualan")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date lamaBerjualan;

    @Column(name = "epd_lama_berjualan_online")
    public String lamaBerjualanOnline;

    @Column(name = "epd_nilai_rata_penjualan")
    public String nilaiRataPenjualan;

    @Column(name = "epd_offline_store_address")
    public String offlineStoreAddress;

    @Column(name = "epd_offline_store_province")
    public String offlineStoreProvince;

    @Column(name = "epd_offline_store_kab_kot")
    public String offlineStoreKabKot;

    @Column(name = "epd_offline_store_kecamatan")
    public String offlineStoreKecamatan;

    @Column(name = "epd_offline_store_kelurahan")
    public String offlineStoreKelurahan;

    @Column(name = "epd_offline_store_postal_code")
    public String offlineStorePostalCode;

    @Column(name = "epd_mslt_id")
    public Integer msltId = 11;

    @Column(name = "epd_borrower_address")
    public String borrowerAddress;

    @Column(name = "epd_ktp")
    public String ktp = "Y";

    @Column(name = "epd_no_ktp_or_sim")
    public String noKtpOrSim;

    @Column(name = "epd_bank_id")
    public Integer bankId;

    @Column(name = "epd_bank_account_no")
    public String bankAccountNo;

    @Column(name = "epd_bank_account_name")
    public String bankAccountName;

    @Column(name = "epd_disctrict_location")
    public String districtLocation;


}
