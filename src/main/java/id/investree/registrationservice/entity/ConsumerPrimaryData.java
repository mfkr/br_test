package id.investree.registrationservice.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "consumer_primary_data")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ConsumerPrimaryData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cpd_id")
    public Long id;

    @Column(name = "cpd_rd_id")
    public Long registrationId;

    @Column(name = "cpd_number")
    public String number;

    @Column(name = "cpd_va_number")
    public String vaNumber;

    @Column(name = "cpd_partnertship_id")
    public Integer partnershipId;

    @Column(name = "cpd_full_name")
    public String fullName;

    @Column(name = "cpd_prefix_title")
    public String prefixTitle;

    @Column(name = "cpd_first_name")
    public String firstName;

    @Column(name = "cpd_middle_name")
    public String middleName;

    @Column(name = "cpd_last_name")
    public String lastName;

    @Column(name = "cpd_suffix_title")
    public String suffixName;

    @Column(name = "cpd_initial")
    public String initial;

    @Column(name = "cpd_gender")
    public String gender;

    @Column(name = "cpd_salutation")
    public String salutation;

    @Column(name = "cpd_email_address")
    public String emailAddress;

    @Column(name = "cpd_mobile_prefix")
    public String mobilePrefix;

    @Column(name = "cpd_mobile_number")
    public String mobileNumber;

    @Column(name = "cpd_activation_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date activationDate;

    @Column(name = "cpd_fill_finish_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public String fillFinishDate;

    @Column(name = "cpd_last_login")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date lastLogin;

    @Column(name = "cpd_login_attempt")
    public Integer loginAttemp = 1;

    @Column(name = "cpd_pob")
    public String pob;

    @Column(name = "cpd_dob")
    public String dob;

    @Column(name = "cpd_phone_number")
    public String phoneNumber;

    @Column(name = "cpd_ktp")
    public String ktp;

    @Column(name = "cpd_ktp_file")
    public String ktpFile;

    @Column(name = "cpd_ktp_expired")
    public String ktpExpired;

    @Column(name = "cpd_npwp")
    public String npwp;

    @Column(name = "cpd_npwp_file")
    public String npwpFile;

    @Column(name = "cpd_cover_tabungan")
    public String coverTabungan = "";

    @Column(name = "cpd_educational")
    public String educational;

    @Column(name = "cpd_marriage_status")
    public String marriageStatus;

    @Column(name = "cpd_relation_name")
    public String relationName;

    @Column(name = "cpd_amount_of_childs")
    public Integer amountOfChilds = 0;

    @Column(name = "cpd_amount_of_dependent")
    public Integer amountOfDependent = 0;

    @Column(name = "cpd_active_hp")
    public String activeHp;

    @Column(name = "cpd_dom_same_ktp")
    public String domSameKtp = "N";

    @Column(name = "cpd_dom_address")
    public String domAddress;

    @Column(name = "cpd_dom_province")
    public String domProvince;

    @Column(name = "cpd_dom_kab_kot")
    public String domKabKot;

    @Column(name = "cpd_dom_kecamatan")
    public String domKecamatan;

    @Column(name = "cpd_dom_kelurahan")
    public String domKelurahan;

    @Column(name = "cpd_dom_postal_code")
    public String domPostalCode;

    @Column(name = "cpd_ktp_address")
    public String ktpAddress;

    @Column(name = "cpd_ktp_province")
    public String ktpProvince;

    @Column(name = "cpd_ktp_kab_kot")
    public String ktpKabKot;

    @Column(name = "cpd_ktp_kecamatan")
    public String ktpKecamatan;

    @Column(name = "cpd_ktp_kelurahan")
    public String ktpKelurahan;

    @Column(name = "cpd_ktp_postal_code")
    public String ktpPostalCode;

    @Column(name = "cpd_kerabat_name")
    public String kerabatName;

    @Column(name = "cpd_kerabat_relation")
    public String kerabatRelation;

    @Column(name = "cpd_kerabat_phone")
    public String kerabatPhone;

    @Column(name = "cpd_kerabat_hp")
    public String kerabatHp;

    @Column(name = "cpd_kerabat_address")
    public String kerabatAddress;

    @Column(name = "cpd_kerabat_province")
    public String kerabatProvince;

    @Column(name = "cpd_kerabat_kab_kot")
    public String kerabatKabKot;

    @Column(name = "cpd_kerabat_kecamatan")
    public String kerabatKecamatan;

    @Column(name = "cpd_kerabat_kelurahan")
    public String kerabatKelurahan;

    @Column(name = "cpd_kerabat_postal_code")
    public String kerabatPostalCode;

    @Column(name = "cpd_bank_name_id")
    public Integer bankNameId;

    @Column(name = "cpd_bank_account_name")
    public String bankAccountName;

    @Column(name = "cpd_bank_account_no")
    public String bankAccountNo;

    @Column(name = "cpd_fb_id")
    public String fbId;

    @Column(name = "cpd_fb_username")
    public String fbUsername;

    @Column(name = "cpd_last_lenddo_id")
    public String lastLenddoId;

    @Column(name = "cpd_created_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date createdDate;

    @Column(name = "cpd_ip_address")
    public String ipAddress;

    @Column(name = "cpd_no_of_loan")
    public Integer noOfLoan;

    @Column(name = "cpd_borrower_status")
    public Integer borrowerStatus;

    @Column(name = "cpd_right_data")
    public String rightData = "D";

    @Column(name = "cpd_checkmark")
    public String checkmark = "D";

    @Column(name = "cpd_latest_updated")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date latestUpdated;

    @Column(name = "cpd_nik")
    public String nik;

    @Column(name = "cpd_resign_status")
    public String regisnStatus = "N";

    @Column(name = "cpd_activated_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date activatedDate;

}
