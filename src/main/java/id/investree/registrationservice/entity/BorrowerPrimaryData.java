package id.investree.registrationservice.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "borrower_primary_data")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BorrowerPrimaryData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "bpd_id")
    public Long id;

    @Column(name = "bpd_rd_id")
    public Long registrationId;

    @Column(name = "bpd_number")
    public String number;

    @Column(name = "bpd_va_number")
    public String vaNumber;

    @Column(name = "bpd_full_name")
    public String fullName;

    @Column(name = "bpd_prefix_title")
    public String prefixTitle;

    @Column(name = "bpd_first_name")
    public String firstName;

    @Column(name = "bpd_middle_name")
    public String middleName;

    @Column(name = "bpd_last_name")
    public String lastName;

    @Column(name = "bpd_suffix_title")
    public String suffixName;

    @Column(name = "bpd_gender")
    public String gender;

    @Column(name = "bpd_salutation")
    public String salutation;

    @Column(name = "bpd_email_address")
    public String emailAddress;

    @Column(name = "bpd_mobile_prefix")
    public String mobilePrefix;

    @Column(name = "bpd_mobile_number")
    public String mobileNumber;

    @Column(name = "bpd_activation_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date activationDate;

    @Column(name = "bpd_fill_finish_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date fillFinishDate;

    @Column(name = "bpd_last_login")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date lastLogin;

    @Column(name = "bpd_login_attempt")
    public Integer loginAttemp = 1;

    @Column(name = "bpd_pob")
    public String pob;

    @Column(name = "bpd_dob")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date dob;

    @Column(name = "bpd_phone_number")
    public String phoneNumber;

    @Column(name = "bpd_ktp")
    public String ktp;

    @Column(name = "bpd_ktp_file")
    public String ktpFile;

    @Column(name = "bpd_selfie_ktp_file")
    public String selfieKtpFile;

    @Column(name = "bpd_ktp_expired")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date ktpExpired;

    @Column(name = "bpd_kk_file")
    public String kkFile;

    @Column(name = "bpd_npwp_no")
    public String npwpNumber;

    @Column(name = "bpd_company_type")
    public Integer companyType;

    @Column(name = "bpd_company_name")
    public String companyName;

    @Column(name = "bpd_company_initial")
    public String companyInitial;

    @Column(name = "bpd_company_industry_cat_id")
    public Integer companyIndustryCategoryId;

    @Column(name = "bpd_company_industry_type_id")
    public Integer companyIndustryTypeId;

    @Column(name = "bpd_company_year_start")
    public Integer companyYearStart;

    @Column(name = "bpd_company_desc")
    public String companyDesc;

    @Column(name = "bpd_company_position")
    public Integer companyPosition;

    @Column(name = "bpd_number_of_employees")
    public Integer numberOfEmployee;

    @Column(name = "bpd_company_address")
    public String companyAddress;

    @Column(name = "bpd_company_phone")
    public String companyPhone;

    @Column(name = "bpd_company_fax")
    public String companyFax;

    @Column(name = "bpd_company_kelurahan")
    public String companyKelurahan;

    @Column(name = "bpd_company_kecamatan")
    public String companyKecamatan;

    @Column(name = "bpd_company_kab_kot")
    public String companyKabKot;

    @Column(name = "bpd_company_province")
    public String companyProvince;

    @Column(name = "bpd_company_postal_code")
    public String companyPostalCode;

    @Column(name = "bpd_domicile_address")
    public String domicileAddress;

    @Column(name = "bpd_domicile_phone")
    public String domicilePhone;

    @Column(name = "bpd_domicile_kelurahan")
    public String domicileKelurahan;

    @Column(name = "bpd_domicile_kecamatan")
    public String domicileKecamatan;

    @Column(name = "bpd_domicile_kab_kot")
    public String domicileKabKot;

    @Column(name = "bpd_domicile_province")
    public String domicileProvince;

    @Column(name = "bpd_domicile_postal_code")
    public String domicilePostalCode;

    @Column(name = "bpd_website_url")
    public String websiteUrl;

    @Column(name = "bpd_cdoc_npwp")
    public String cdocNpwp;

    @Column(name = "bpd_cdoc_npwp_file")
    public String cdocFile;

    @Column(name = "bpd_cdoc_siup_no")
    public String cdocSiupNumber;

    @Column(name = "bpd_cdoc_siup_register_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date cdocSiupRegisterDate;

    @Column(name = "bpd_cdoc_siup_expired_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date cdocSiupExpiredDate;

    @Column(name = "bpd_cdoc_siup_file")
    public String cdocSiupFile;

    @Column(name = "bpd_cdoc_tdp_no")
    public String cdocTdpNumber;

    @Column(name = "bpd_cdoc_tdp_register_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date cdocTdpRegisterDate;

    @Column(name = "bpd_cdoc_tdp_expired_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date cdocTdpExpiredDate;

    @Column(name = "bpd_cdoc_tdp_file")
    public String cdocTdpFile;

    @Column(name = "bpd_cdoc_akta_pendirian_no")
    public String cdocAktaPendirianNumber;

    @Column(name = "bpd_cdoc_akta_pendirian_file")
    public String cdocAktaPendirianFile;

    @Column(name = "bpd_cdoc_akta_pendirian_register_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date cdocAktaPendirianRegisterDate;

    @Column(name = "bpd_cdoc_menhumkam_register_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date cdocMenhumkamRegisterDate;

    @Column(name = "bpd_cdoc_akta_perubahan_no")
    public String cdocAktaPerubahanNumber;

    @Column(name = "bpd_cdoc_akta_perubahan_file")
    public String cdocAktaPerubahanFile;

    @Column(name = "bpd_cdoc_akta_perubahan_register_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date cdocAktaPerubahanRegisterDate;

    @Column(name = "bpd_cdoc_sk_menhumkam")
    public String cdocSkMenhumkam;

    @Column(name = "bpd_cdoc_sk_menhumkam_file")
    public String cdocSkMenhumkamFile;

    @Column(name = "bpd_cdoc_skdp")
    public String cdocSkdp;

    @Column(name = "bpd_cdoc_skdp_file")
    public String cdocSkdpFile;

    @Column(name = "bpd_cdoc_laporan_keuangan")
    public String cdocLaporanKeuangan;

    @Column(name = "bpd_cdoc_company_profile")
    public String cdocCompanyProfile;

    @Column(name = "bpd_cdoc_rekening_koran")
    public String cdocRekeningKoran;

    @Column(name = "bpd_cdoc_skdp_register_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date cdocSkdpRegisterDate;

    @Column(name = "bpd_cdoc_skdp_expired_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date cdocSkdpExpiredDate;

    @Column(name = "bpd_company_bank_name_id")
    public Integer companyBankNameId;

    @Column(name = "bpd_company_bank_account_name")
    public String companyBankAccountName;

    @Column(name = "bpd_company_bank_number")
    public String companyBankNumber;

    @Column(name = "bpd_investree_bank_va_name_id")
    public Integer investreeBankVaNameId;

    @Column(name = "bpd_investree_bank_va_client_name")
    public String investreeBankVaClientName;

    @Column(name = "bpd_investree_bank_va_no")
    public String investreeBankVaNumber;

    @Column(name = "bpd_buku_tabungan_file")
    public String bukuTabunganFile;

    @Column(name = "bpd_created_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date createdDate;

    @Column(name = "bpd_ip_address")
    public String ipAddress;

    @Column(name = "bpd_no_of_loan")
    public Integer numberOfLoan;

    @Column(name = "bpd_borrower_status")
    public Integer borrowerStatus;

    @Column(name = "bpd_reject_reason_id")
    public Integer rejectReasonId;

    @Column(name = "bpd_reject_datetime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date rejectDateTime;

    @Column(name = "bpd_reject_note")
    public String rejectNote;

    @Column(name = "bpd_right_data")
    public String rightData = "D";

    @Column(name = "bpd_checkmark")
    public String checkMark = "D";

    @Column(name = "bpd_registered_va")
    public String registeredVa = "D";

    @Column(name = "bpd_registered_va_syariah")
    public String registeredVaSyariah = "D";

    @Column(name = "bpd_approve_to_dashboard")
    public String approveToDashboard = "N";

    @Column(name = "bpd_completed_docs")
    public String completeDocs = "N";

    @Column(name = "bpd_latest_updated")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date latestUpdated;

    @Column(name = "bpd_activated_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date activatedDate;

    @Column(name = "bpd_no_dpa")
    public String noDpa;

    @Column(name = "bpd_tgl_dpa")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date tanggalDpa;

    @Column(name = "bpd_tgl_dpa_file_upload_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date tanggalDpaFileUploadDate;

    @Column(name = "bpd_tgl_dpa_file")
    public String tanggalDpaFile;

    @Column(name = "bpd_rm_name")
    public Integer rmName;

    @Column(name = "bpd_alamat_dpa")
    public String alamatDpa;

    @Column(name = "bpd_client_name")
    public String clientName;

    @Column(name = "bpd_client_contract")
    public String clientContract;

    @Column(name = "bpd_email_direksi")
    public String emailDireksi;

    @Column(name = "bpd_platform_dpa")
    public Double platformDpa;

    @Column(name = "bpd_industry_nob_id")
    public Integer industryNobId;

    @Column(name = "bpd_loan_type")
    public String loanType;

    @Column(name = "bpd_sub_loan_type")
    public String subLoanType;

    @Column(name = "bpd_request_for_syariah")
    public String requestForSyariah = "D";

    @Column(name = "bpd_desc_from_investree")
    public String descFromInvestree;

    @Column(name = "bpd_dpa_fill_finish_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date dpaFillFinishDate;

    @Column(name = "bpd_referral_schema")
    public Integer referralSchema;

    @Column(name = "bpd_jamkrindo")
    public String jamkrindo = "N";

    @Column(name = "bpd_requested_pefindo")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public Date requestedPefindo;

    @Column(name = "bpd_status_pefindo")
    public String statusPefindo = "N";

}
