package id.investree.registrationservice.utils;

public class StringUtils {

    public static Integer toInt(String numberInString) {
        try {
            return Integer.parseInt(numberInString);
        } catch (Exception ex) {
            return 0;
        }
    }
}
