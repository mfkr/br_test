/*
Created: 03/12/2018
This Jenkinsfile is controling INVSTREE CI/CD PIPELINE. ver. 0.0.4 (Release Candidate).
Maintained by: Investree Devops Team
*/
def notifySlack(String buildStatus = 'STARTED') {
    // Build status of null means success.
    buildStatus = buildStatus ?: 'SUCCESS'
    def color
    if (buildStatus == 'STARTED') {
        color = '#D4DADF'
    } else if (buildStatus == 'SUCCESS') {
        color = '#BDFFC3'
    } else if (buildStatus == 'UNSTABLE') {
        color = '#FFFE89'
    } else {
        color = '#FF9FA1'
    }
    //slack message
    def msg = "${buildStatus}: `${env.JOB_NAME}` #${env.BUILD_NUMBER}:\n${env.BUILD_URL}"
    slackSend(color: color, message: msg)
}

def label = "mypod-${UUID.randomUUID().toString()}"
podTemplate(label: label,
        containers: [
        containerTemplate(
          name: 'maven',
          image: 'maven:3.5.2-jdk-8',
          command: 'cat',
          ttyEnabled: true),
        containerTemplate(
          name: 'kubectl',
          image: 'gcr.io/cloud-builders/kubectl',
          command: 'cat',
          ttyEnabled: true),
        containerTemplate(
          name: 'docker',
          image: 'docker',
          command: 'cat',
          ttyEnabled: true),
        ],
        volumes: [
        hostPathVolume(hostPath: '/var/run/docker.sock',
        mountPath: '/var/run/docker.sock')]){
  node(label){
    BITBUCKET_URL  = "https://mfkr@bitbucket.org/mfkr/br_test.git" //Bitbucket repository url
    DOCKER_REGISTRY_URL = 'http://registry-intl.ap-southeast-5.aliyuncs.com' //Alicloud container registry URL
    DOCKER_REGISTRY = 'registry-intl.ap-southeast-5.aliyuncs.com' //Alicloud container registry name
    DOCKER_IMAGE_NAME = 'devopstestprivate' //Alicloud docker repository name
    DOCKER_IMAGE_VERSION = "latest" //Docker images version
    MS_NAME = "devopstestprivate" //microservice name , it's value by default must be same like DOCKER_IMAGE_NAME
    JNKNS_MST_NAME = "jenkins-master-jenkins-6c6b56966b-m62vg" //jenkins master name
    DEPLOY_YML_SRC_PATH = "/var/jenkins_home/k8sdeployment" //dir path of docker-compose .yaml file
    DEPLOY_YML_FILE = "k8sdeploy.yaml" //docker-compose .yaml file
    APP_YML_SRC_PATH = "/var/jenkins_home/credentials" //source path of credential/.yml file
    APP_YML_TRGT_PATH = "src/main/resources" //target path of credential/.yml file, used when running  maven build
    DOCKERFILE_PATH = "/var/jenkins_home/Docker" //path of dockerfile, used when running docker build
    DEV_DOCKFILE = "jarDockerfile" //name of Dockerfile when building docker image for development environment
    STG_DOCKFILE = "jarDockerfile" //name of Dockerfile when building docker image for staging environment
    MST_DOCKFILE = "jarDockerfile" //name of Dockerfile when biilding docker image for master/prelive environment
    DEV_YML = "application.yml" //.yml file name when running maven build for development environment
    DEF_YML = "application.yml"
    //STG_YML = "test.registration.application.yml"
    //MST_YML = "prelive.registration.application.yml"
    STG_YML = "test.registration.application.yml" //.yml file name when running maven build for staging environment test.registration.application.yml
    MST_YML = "prelive.registration.application.yml" //.yml file name when running maven build for master/prelive environment
    JNKNS_CRED_ID = "ekahePrivateBBCredID" //Jenkins credential ID for connect to BB
    ALI_REG_CRED_ID = "ali2ContReg_CredID" //Alicloud container registry credential ID
    ALI_KUBECTL_CRED_ID = "ali2DevKubeClust_CredID" //Alicloud kubernetes cluster credential ID
    ALI_KUBE_SERVER_URL = "https://10.5.1.194:6443"//Alicloud kubernetes cluster server URL (development cluster)
    //ALI_KUBE_SERVER_DEV_URL = ""
    //ALI_KUBE_SERVER_TEST_URL = ""
    //ALI_KUBE_SERVER_PREPROD = ""
    CONTEXT_NAME = "invstrDevCorReg" //Alicloud kubernetes cluster context name
    API_PATH = "api/v1/registration/borrower" //deployed micro services test path
    JEN_NAMESPACE = "jenkins" //Alicloud kubernetes cluster for jenkins namespace
    DEF_NAMESPACE = "default" //Alicloud kubernetes cluster for default namespace
    DEV_NAMESPACE = "invstr-dev" //Alicloud kubernetes cluster for development environment namespace
    STG_NAMESPACE = "invstr-test" //Alicloud kubernetes cluster for staging environment namespace //next edit
    MST_NAMESPACE = "invstr-prelive" //Alicloud kubernetes cluster for master/prelive environment namespace //next edit
    CR_DEV_NAMESPACE = "invstr-devel"
    CR_STG_NAMESPACE  = "invstr-staging"
    CR_MST_NAMESPACE  = "invstr-master"
    DEV_BRANCH = "origin/develop" //Bitbucket develop branch name
    STG_BRANCH = "origin/staging" //Bitbucket staging branch name
    MST_BRANCH = "origin/master" //Bitbucket master branch name
    DEV_SVC_PREFIX = "dev" //prefix for development micro service loadBalancer
    STG_SVC_PREFIX = "test" //prefix for staging micro service loadBalancer
    MST_SVC_PREFIX = "release" //prefix for master/prelive micro service loadBalancer
    SERVICE_PORT = "8080" //micro service exposed port
    PORT_LB = "8080" //loadBalancer port
    TARGET_PORT_LB = "8080" //loadBalancer exposed port
    //pull source codes and detect the branch
    stage('Pull Source Codes'){
      scmVars = checkout([$class: 'GitSCM', branches: [[name: ':^(?!(origin/prefix)).*']],
          userRemoteConfigs: [[credentialsId: "${JNKNS_CRED_ID}", url: "${BITBUCKET_URL}"]]])
          env.GIT_COMMIT = scmVars.GIT_COMMIT
          env.GIT_BRANCH = scmVars.GIT_BRANCH
          echo "commit name: ${env.GIT_COMMIT}"
          echo "branch name: ${env.GIT_BRANCH}"
          SERVICE_NAME = sh(script: "sed -n \'s:.*<artifactId>\\(.*\\)</artifactId>.*:\\1:p\' pom.xml  | head -1 | awk -F'-' '{print \$1}'", returnStdout: true).trim()
    }

    if (env.GIT_BRANCH == "${DEV_BRANCH}"){
    try{
      notifySlack()
      echo "Expected branch: ${DEV_BRANCH} <---> Actual branch: ${env.GIT_BRANCH}"
      echo "This Branch is: ${env.GIT_BRANCH}"
      echo "------------------------------------------------------------------------"
      echo "build from this branch will be deployed to kubernetes development cluster"
      //inject .yml file
      stage('Injecting .yml File to src'){
        container('kubectl'){
          withKubeConfig([credentialsId:"${ALI_KUBECTL_CRED_ID}", serverUrl:"${ALI_KUBE_SERVER_URL}", contextName:"${CONTEXT_NAME}"]){
              echo "Injecting src ..."
              sh "kubectl cp ${JEN_NAMESPACE}/${JNKNS_MST_NAME}:${APP_YML_SRC_PATH}/${DEV_YML} ${APP_YML_TRGT_PATH}/${DEF_YML}"
              sh "kubectl cp ${JEN_NAMESPACE}/${JNKNS_MST_NAME}:${DOCKERFILE_PATH}/${DEV_DOCKFILE} ."
              sh "ls -l ${APP_YML_TRGT_PATH}"
          }
        }
      }

      //Build .jar file using maven
      stage('Build .JAR'){
        container('maven'){
          pom = readMavenPom file: 'pom.xml'
          version = pom.version.replace("-SNAPSHOT", ".${currentBuild.number}")
          sh "echo ${version}"
          sh 'mvn --version'
          sh 'mvn -e clean package'
          sh "sed -i -e 's/jarversion/${version}/' ${DEV_DOCKFILE}"
          sh "cat ${DEV_DOCKFILE}"
          sh 'ls -l target'
        }
      }

      //Build Tomcat then input the .jar file into it, based on Dockerfile
      stage('Build Tomcat Docker and Import .jar file'){
        container('docker'){
          sh "docker build --rm -t ${DOCKER_IMAGE_NAME} -f ${DEV_DOCKFILE} --no-cache ."
          //sh "docker build --rm -t ${DOCKER_IMAGE_NAME}-bak -f ${DEV_DOCKFILE} --no-cache ."
        }
      }

      //Push the Tomcat image to Alicloud Container Registry "registration"
      stage('Push Tomcat Docker Image'){
        container('docker'){
          withDockerRegistry([ credentialsId: "${ALI_REG_CRED_ID}", url: "${DOCKER_REGISTRY_URL}/${CR_DEV_NAMESPACE}/${DOCKER_IMAGE_NAME}" ]) {
            sh "docker tag ${DOCKER_IMAGE_NAME}:latest ${DOCKER_REGISTRY}/${CR_DEV_NAMESPACE}/${DOCKER_IMAGE_NAME}:${env.BUILD_NUMBER}"
            sh "docker push ${DOCKER_REGISTRY}/${CR_DEV_NAMESPACE}/${DOCKER_IMAGE_NAME}:${env.BUILD_NUMBER}"
            //sh "docker tag ${DOCKER_IMAGE_NAME}-bak:${env.BUILD_NUMBER} ${DOCKER_REGISTRY}/${CR_DEV_NAMESPACE}/${DOCKER_IMAGE_NAME}:${env.BUILD_NUMBER}-backup"
            //sh "docker push ${DOCKER_REGISTRY}/${CR_DEV_NAMESPACE}/${DOCKER_IMAGE_NAME}:${env.BUILD_NUMBER}-backup"
            sh "docker images -a ${DOCKER_REGISTRY}/${CR_DEV_NAMESPACE}/${DOCKER_IMAGE_NAME}"
            sh "docker images -a ${DOCKER_REGISTRY}/${CR_DEV_NAMESPACE}/${DOCKER_IMAGE_NAME}:${env.BUILD_NUMBER} -q"
            sh "docker rmi -f \$(docker images -a ${DOCKER_REGISTRY}/${CR_DEV_NAMESPACE}/${DOCKER_IMAGE_NAME}:${env.BUILD_NUMBER} -q)"
            //sh "docker rmi -f \$(docker images -a ${DOCKER_REGISTRY}/${CR_DEV_NAMESPACE}/${DOCKER_IMAGE_NAME}:${env.BUILD_NUMBER}-backup -q)"
          }
        }
      }

      //Deploy Tomcat:lates to development environment
      stage('Deploy to Development'){
        container('kubectl'){
          withKubeConfig([credentialsId:"${ALI_KUBECTL_CRED_ID}", serverUrl:"${ALI_KUBE_SERVER_URL}", contextName:"${CONTEXT_NAME}"]){
            echo "Listing Develop branch deployment.."
            LIST_DEPLOYMENT = sh(script: "kubectl get deployment --namespace=${DEV_NAMESPACE} | grep \"${DEV_SVC_PREFIX}-${MS_NAME}\" | wc -l", returnStdout: true).trim()
            echo "Detected deployment: ${LIST_DEPLOYMENT}"
            //LATEST_DEPLOYMENT_BN = sh(script: "kubectl get deployment --namespace=${DEV_NAMESPACE} | grep \"${DEV_SVC_PREFIX}-${MS_NAME}\" | awk -F'-' '{print \$3}' | awk '{print \$1}' | head -n 1", returnStdout: true).trim()
            if (LIST_DEPLOYMENT == "1"){
              echo "Updating ${DEV_SVC_PREFIX}-${MS_NAME}service in Development environment..."
              sh "kubectl set image --namespace=${DEV_NAMESPACE} deployment/${DEV_SVC_PREFIX}-${MS_NAME} ${DEV_SVC_PREFIX}-${MS_NAME}=registry-intl-vpc.ap-southeast-5.aliyuncs.com/${CR_DEV_NAMESPACE}/${DOCKER_IMAGE_NAME}:${env.BUILD_NUMBER}"
              sh "kubectl rollout status --namespace=${DEV_NAMESPACE} deployments/${DEV_SVC_PREFIX}-${MS_NAME}"
              sh "kubectl get pods --namespace=${DEV_NAMESPACE}"
              SVC_URL = sh(script: "kubectl get svc --namespace=${DEV_NAMESPACE} | grep \"${DEV_SVC_PREFIX}-${MS_NAME}\" | awk '{print \$4}'", returnStdout: true).trim()
            }
            else{
              echo "We are Deploying ${DEV_SVC_PREFIX}-${MS_NAME}:latest to Developent environment in Kubernetes Cluster"
              sh "kubectl cp ${JEN_NAMESPACE}/${JNKNS_MST_NAME}:${DEPLOY_YML_SRC_PATH}/${DEPLOY_YML_FILE} ."
              sh "sed -i -e 's/msname/${DEV_SVC_PREFIX}-${MS_NAME}/' ${DEPLOY_YML_FILE}"
              sh "sed -i -e 's/k8snamespace/${DEV_NAMESPACE}/' ${DEPLOY_YML_FILE}"
              sh "sed -i -e 's/reponame/${DOCKER_IMAGE_NAME}/' ${DEPLOY_YML_FILE}"
              sh "sed -i -e 's/imageversion/${env.BUILD_NUMBER}/' ${DEPLOY_YML_FILE}"
              sh "sed -i -e 's/conregnms/${CR_DEV_NAMESPACE}/' ${DEPLOY_YML_FILE}"
              sh "kubectl create -f ${DEPLOY_YML_FILE}"
              sh "kubectl expose deployment --namespace=${DEV_NAMESPACE} ${DEV_SVC_PREFIX}-${MS_NAME} --type=LoadBalancer --port=${PORT_LB} --target-port=${TARGET_PORT_LB}"
              sh "kubectl get pods --namespace=${DEV_NAMESPACE}"
              sh "kubectl get deployment --namespace=${DEV_NAMESPACE}"
              sh "kubectl get svc --namespace=${DEV_NAMESPACE}"
              echo "Running the Service and Getting URL: ..."
              SVC_EXTERNAL_IP = sh(script: "kubectl get svc --namespace=${DEV_NAMESPACE} | grep \"${DEV_SVC_PREFIX}-${MS_NAME}\" | awk '{print \$4}'", returnStdout: true).trim()
              SVC_INTERNAL_IP = sh(script: "kubectl get svc --namespace=${DEV_NAMESPACE} | grep \"${DEV_SVC_PREFIX}-${MS_NAME}\" | awk '{print \$3}'", returnStdout: true).trim()
              SVC_URL = sh(script: "kubectl get svc --namespace=${DEV_NAMESPACE} | grep \"${DEV_SVC_PREFIX}-${MS_NAME}\" | awk '{print \$4}'", returnStdout: true).trim()
              echo "Your External IP is: ${SVC_EXTERNAL_IP}"
              echo "Yout Internal IP is: ${SVC_INTERNAL_IP}"
              echo "Running the Service and Getting URL: ...wait for 75 seconds!"
              sh 'sleep 75'
              echo "This is your URL: http://${SVC_URL}:${PORT_LB}/${API_PATH}"
            }
          }
        }
      }
    }//try
    catch (e) {
      currentBuild.result = 'FAILURE'
      throw e
      } finally {
      notifySlack(currentBuild.result)
      slackSend color: 'good', message: "${SERVICE_NAME}-${version} has been deployed to Development environment"
      slackSend color: 'good', message: "This is your URL: http://${SVC_URL}:${PORT_LB}/${API_PATH}"
    }
    }
    else if (env.GIT_BRANCH == "${STG_BRANCH}"){
    try{
      notifySlack()
      echo "Expected branch: ${STG_BRANCH} <---> Actual branch: ${env.GIT_BRANCH}"
      echo "This Branch is: ${env.GIT_BRANCH}"
      echo "------------------------------------------------------------------------"
      echo "build from this branch will be deployed to kubernetes staging cluster"

      //inject .yml file
      stage('Injecting .yml File to src'){
        container('kubectl'){
          withKubeConfig([credentialsId:"${ALI_KUBECTL_CRED_ID}", serverUrl:"${ALI_KUBE_SERVER_URL}", contextName:"${CONTEXT_NAME}"]){
              echo "Injecting src ..."
              sh "kubectl cp ${JEN_NAMESPACE}/${JNKNS_MST_NAME}:${APP_YML_SRC_PATH}/${STG_YML} ${APP_YML_TRGT_PATH}/${DEF_YML}"
              sh "kubectl cp ${JEN_NAMESPACE}/${JNKNS_MST_NAME}:${DOCKERFILE_PATH}/${STG_DOCKFILE} ."
              sh "ls -l ${APP_YML_TRGT_PATH}"
          }
        }
      }
      //Build .jar file using maven
      stage('Build .JAR'){
        container('maven'){
          pom = readMavenPom file: 'pom.xml'
          version = pom.version.replace("-SNAPSHOT", ".${currentBuild.number}")
          sh "echo ${version}"
          sh 'mvn --version'
          sh 'mvn -e clean package'
          sh "sed -i -e 's/jarversion/${version}/' ${STG_DOCKFILE}"
          sh "cat ${STG_DOCKFILE}"
          sh 'ls -l target'
        }
      }

      //Build Tomcat then input the .jar file into it, based on Dockerfile
      stage('Build Tomcat Docker and Import .jar file'){
        container('docker'){
          sh "docker build --rm -t ${DOCKER_IMAGE_NAME} -f ${STG_DOCKFILE} --no-cache ."
          //sh "docker build --rm -t ${DOCKER_IMAGE_NAME}-bak -f ${STG_DOCKFILE} --no-cache ."
        }
      }

      //Push the Tomcat image to Alicloud Container Registry "registration"
      stage('Push Tomcat Docker Image'){
        container('docker'){
          withDockerRegistry([ credentialsId: "${ALI_REG_CRED_ID}", url: "${DOCKER_REGISTRY_URL}/${CR_STG_NAMESPACE }/${DOCKER_IMAGE_NAME}" ]){
            sh "docker tag ${DOCKER_IMAGE_NAME}:latest ${DOCKER_REGISTRY}/${CR_STG_NAMESPACE}/${DOCKER_IMAGE_NAME}:${env.BUILD_NUMBER}"
            sh "docker push ${DOCKER_REGISTRY}/${CR_STG_NAMESPACE}/${DOCKER_IMAGE_NAME}:${env.BUILD_NUMBER}"
            //sh "docker tag ${DOCKER_IMAGE_NAME}-bak:${env.BUILD_NUMBER} ${DOCKER_REGISTRY}/${CR_STG_NAMESPACE}/${DOCKER_IMAGE_NAME}:${env.BUILD_NUMBER}-backup"
            //sh "docker push ${DOCKER_REGISTRY}/${CR_STG_NAMESPACE}/${DOCKER_IMAGE_NAME}:${env.BUILD_NUMBER}-backup"
            sh "docker images -a ${DOCKER_REGISTRY}/${CR_STG_NAMESPACE}/${DOCKER_IMAGE_NAME}"
            sh "docker images -a ${DOCKER_REGISTRY}/${CR_STG_NAMESPACE}/${DOCKER_IMAGE_NAME}:${env.BUILD_NUMBER} -q"
            sh "docker rmi -f \$(docker images -a ${DOCKER_REGISTRY}/${CR_STG_NAMESPACE}/${DOCKER_IMAGE_NAME}:${env.BUILD_NUMBER} -q)"
            //sh "docker rmi -f \$(docker images -a ${DOCKER_REGISTRY}/${CR_STG_NAMESPACE}/${DOCKER_IMAGE_NAME}:${env.BUILD_NUMBER}-backup -q)"
          }
        }
      }

      //Deploy Tomcat:lates to staging environment
      stage('Deploy to Staging'){
        container('kubectl'){
          withKubeConfig([credentialsId:"${ALI_KUBECTL_CRED_ID}", serverUrl:"${ALI_KUBE_SERVER_URL}", contextName:"${CONTEXT_NAME}"]){
            echo "Listing Staging branch deployment.."
            LIST_DEPLOYMENT = sh(script: "kubectl get deployment --namespace=${STG_NAMESPACE} | grep \"${STG_SVC_PREFIX}-${MS_NAME}\" | wc -l", returnStdout: true).trim()
            echo "Detected deployment: ${LIST_DEPLOYMENT}"
            //LATEST_DEPLOYMENT_BN = sh(script: "kubectl get deployment --namespace=${STG_NAMESPACE} | grep \"${STG_SVC_PREFIX}-${MS_NAME}\" | awk -F'-' '{print \$3}' | awk '{print \$1}' | head -n 1", returnStdout: true).trim()
            if (LIST_DEPLOYMENT == "1"){
              echo "Updating ${STG_SVC_PREFIX}-${MS_NAME}service in Staging environment..."
              sh "kubectl set image --namespace=${STG_NAMESPACE} deployment/${STG_SVC_PREFIX}-${MS_NAME} ${STG_SVC_PREFIX}-${MS_NAME}=registry-intl-vpc.ap-southeast-5.aliyuncs.com/${CR_STG_NAMESPACE}/${DOCKER_IMAGE_NAME}:${env.BUILD_NUMBER}"
              sh "kubectl rollout status --namespace=${STG_NAMESPACE} deployments/${STG_SVC_PREFIX}-${MS_NAME}"
              sh "kubectl get pods --namespace=${STG_NAMESPACE}"
              SVC_URL = sh(script: "kubectl get svc --namespace=${STG_NAMESPACE} | grep \"${STG_SVC_PREFIX}-${MS_NAME}\" | awk '{print \$4}'", returnStdout: true).trim()
            }
            else{
              echo "We are Deploying ${STG_SVC_PREFIX}-${MS_NAME}:latest to Staging environment in Kubernetes Cluster"
              sh "kubectl cp ${JEN_NAMESPACE}/${JNKNS_MST_NAME}:${DEPLOY_YML_SRC_PATH}/${DEPLOY_YML_FILE} ."
              sh "sed -i -e 's/msname/${STG_SVC_PREFIX}-${MS_NAME}/' ${DEPLOY_YML_FILE}"
              sh "sed -i -e 's/k8snamespace/${STG_NAMESPACE}/' ${DEPLOY_YML_FILE}"
              sh "sed -i -e 's/reponame/${DOCKER_IMAGE_NAME}/' ${DEPLOY_YML_FILE}"
              sh "sed -i -e 's/imageversion/${env.BUILD_NUMBER}/' ${DEPLOY_YML_FILE}"
              sh "sed -i -e 's/conregnms/${CR_STG_NAMESPACE}/' ${DEPLOY_YML_FILE}"
              sh "kubectl create -f ${DEPLOY_YML_FILE}"
              sh "kubectl expose deployment --namespace=${STG_NAMESPACE} ${STG_SVC_PREFIX}-${MS_NAME} --type=LoadBalancer --port=${PORT_LB} --target-port=${TARGET_PORT_LB}"
              sh "kubectl get pods --namespace=${STG_NAMESPACE}"
              sh "kubectl get deployment --namespace=${STG_NAMESPACE}"
              sh "kubectl get svc --namespace=${STG_NAMESPACE}"
              echo "Running the Service and Getting URL: ..."
              SVC_EXTERNAL_IP = sh(script: "kubectl get svc --namespace=${STG_NAMESPACE} | grep \"${STG_SVC_PREFIX}-${MS_NAME}\" | awk '{print \$4}'", returnStdout: true).trim()
              SVC_INTERNAL_IP = sh(script: "kubectl get svc --namespace=${STG_NAMESPACE} | grep \"${STG_SVC_PREFIX}-${MS_NAME}\" | awk '{print \$3}'", returnStdout: true).trim()
              SVC_URL = sh(script: "kubectl get svc --namespace=${STG_NAMESPACE} | grep \"${STG_SVC_PREFIX}-${MS_NAME}\" | awk '{print \$4}'", returnStdout: true).trim()
              echo "Your External IP is: ${SVC_EXTERNAL_IP}"
              echo "Yout Internal IP is: ${SVC_INTERNAL_IP}"
              echo "Running the Service and Getting URL: ...wait for 75 seconds!"
              sh 'sleep 75'
              echo "This is your URL: http://${SVC_URL}:${PORT_LB}/${API_PATH}"
            }
          }
        }
      }
    }//try
    catch (e) {
      currentBuild.result = 'FAILURE'
      throw e
      } finally {
      notifySlack(currentBuild.result)
      slackSend color: 'good', message: "${SERVICE_NAME}-${version} has been deployed to Test environment"
      slackSend color: 'good', message: "This is your URL: http://${SVC_URL}:${PORT_LB}/${API_PATH}"
    }
    }
    else if (env.GIT_BRANCH == "${MST_BRANCH}"){
    try{
      notifySlack()
      echo "Expected branch: ${MST_BRANCH} <---> Actual branch: ${env.GIT_BRANCH}"
      echo "This Branch is: ${env.GIT_BRANCH}"
      echo "------------------------------------------------------------------------"
      echo "build from this branch will be deployed to kubernetes pre-Live cluster"

      //inject .yml file
      stage('Injecting .yml File to src'){
        container('kubectl'){
          withKubeConfig([credentialsId:"${ALI_KUBECTL_CRED_ID}", serverUrl:"${ALI_KUBE_SERVER_URL}", contextName:"${CONTEXT_NAME}"]){
              echo "Injecting src ..."
              sh "kubectl cp ${JEN_NAMESPACE}/${JNKNS_MST_NAME}:${APP_YML_SRC_PATH}/${MST_YML} ${APP_YML_TRGT_PATH}/${DEF_YML}"
              sh "kubectl cp ${JEN_NAMESPACE}/${JNKNS_MST_NAME}:${DOCKERFILE_PATH}/${MST_DOCKFILE} ."
              sh "ls -l ${APP_YML_TRGT_PATH}"
          }
        }
      }

      //Build .jar file using maven
      stage('Build .JAR'){
        container('maven'){
          pom = readMavenPom file: 'pom.xml'
          version = pom.version.replace("-SNAPSHOT", ".${currentBuild.number}")
          sh "echo ${version}"
          sh 'mvn --version'
          sh 'mvn -e clean package'
          sh "sed -i -e 's/jarversion/${version}/' ${MST_DOCKFILE}"
          sh "cat ${MST_DOCKFILE}"
          sh 'ls -l target'
        }
      }

      //Build Tomcat then input the .jar file into it, based on Dockerfile
      stage('Build Tomcat Docker and Import .jar file'){
        container('docker'){
          sh "docker build --rm -t ${DOCKER_IMAGE_NAME} -f ${MST_DOCKFILE} --no-cache ."
          //sh "docker build --rm -t ${DOCKER_IMAGE_NAME}-bak -f ${MST_DOCKFILE} --no-cache ."
        }
      }

      //Push the Tomcat image to Alicloud Container Registry "registration"
      stage('Push Tomcat Docker Image'){
        container('docker'){
          withDockerRegistry([ credentialsId: "${ALI_REG_CRED_ID}", url: "${DOCKER_REGISTRY_URL}/${CR_MST_NAMESPACE }/${DOCKER_IMAGE_NAME}" ]){
            sh "docker tag ${DOCKER_IMAGE_NAME}:latest ${DOCKER_REGISTRY}/${CR_MST_NAMESPACE}/${DOCKER_IMAGE_NAME}:${env.BUILD_NUMBER}"
            sh "docker push ${DOCKER_REGISTRY}/${CR_MST_NAMESPACE}/${DOCKER_IMAGE_NAME}:${env.BUILD_NUMBER}"
            //sh "docker tag ${DOCKER_IMAGE_NAME}-bak:${env.BUILD_NUMBER} ${DOCKER_REGISTRY}/${CR_MST_NAMESPACE}/${DOCKER_IMAGE_NAME}:${env.BUILD_NUMBER}-backup"
            //sh "docker push ${DOCKER_REGISTRY}/${CR_MST_NAMESPACE}/${DOCKER_IMAGE_NAME}:${env.BUILD_NUMBER}-backup"
            sh "docker images -a ${DOCKER_REGISTRY}/${CR_MST_NAMESPACE}/${DOCKER_IMAGE_NAME}"
            sh "docker images -a ${DOCKER_REGISTRY}/${CR_MST_NAMESPACE}/${DOCKER_IMAGE_NAME}:${env.BUILD_NUMBER} -q"
            sh "docker rmi -f \$(docker images -a ${DOCKER_REGISTRY}/${CR_MST_NAMESPACE}/${DOCKER_IMAGE_NAME}:${env.BUILD_NUMBER} -q)"
            //sh "docker rmi -f \$(docker images -a ${DOCKER_REGISTRY}/${CR_MST_NAMESPACE}/${DOCKER_IMAGE_NAME}:${env.BUILD_NUMBER}-backup -q)"
          }
        }
      }

      //Deploy Tomcat:lates to pre-live environment
      stage('Deploy to Pre-Live'){
        container('kubectl'){
          withKubeConfig([credentialsId:"${ALI_KUBECTL_CRED_ID}", serverUrl:"${ALI_KUBE_SERVER_URL}", contextName:"${CONTEXT_NAME }"]){
            echo "Listing Pre-live branch deployment.."
            LIST_DEPLOYMENT = sh(script: "kubectl get deployment --namespace=${MST_NAMESPACE} | grep \"${MST_SVC_PREFIX}-${MS_NAME}\" | wc -l", returnStdout: true).trim()
            echo "Detected deployment: ${LIST_DEPLOYMENT}"
            //LATEST_DEPLOYMENT_BN = sh(script: "kubectl get deployment --namespace=${MST_NAMESPACE} | grep \"${MST_SVC_PREFIX}-${MS_NAME}\" | awk -F'-' '{print \$3}' | awk '{print \$1}' | head -n 1", returnStdout: true).trim()
            if (LIST_DEPLOYMENT == "1"){
              echo "Updating ${MST_SVC_PREFIX}-${MS_NAME}service in Pre-live environment..."
              sh "kubectl set image --namespace=${MST_NAMESPACE} deployment/${MST_SVC_PREFIX}-${MS_NAME} ${MST_SVC_PREFIX}-${MS_NAME}=registry-intl-vpc.ap-southeast-5.aliyuncs.com/${CR_MST_NAMESPACE}/${DOCKER_IMAGE_NAME}:${env.BUILD_NUMBER}"
              sh "kubectl rollout status --namespace=${MST_NAMESPACE} deployments/${MST_SVC_PREFIX}-${MS_NAME}"
              sh "kubectl get pods --namespace=${MST_NAMESPACE}"
              SVC_URL = sh(script: "kubectl get svc --namespace=${MST_NAMESPACE} | grep \"${MST_SVC_PREFIX}-${MS_NAME}\" | awk '{print \$4}'", returnStdout: true).trim()
            }
            else{
              echo "We are Deploying ${MST_SVC_PREFIX}-${MS_NAME}:latest to Pre-live environment in Kubernetes Cluster"
              sh "kubectl cp ${JEN_NAMESPACE}/${JNKNS_MST_NAME}:${DEPLOY_YML_SRC_PATH}/${DEPLOY_YML_FILE} ."
              sh "sed -i -e 's/msname/${MST_SVC_PREFIX}-${MS_NAME}/' ${DEPLOY_YML_FILE}"
              sh "sed -i -e 's/k8snamespace/${MST_NAMESPACE}/' ${DEPLOY_YML_FILE}"
              sh "sed -i -e 's/reponame/${DOCKER_IMAGE_NAME}/' ${DEPLOY_YML_FILE}"
              sh "sed -i -e 's/imageversion/${env.BUILD_NUMBER}/' ${DEPLOY_YML_FILE}"
              sh "sed -i -e 's/conregnms/${CR_MST_NAMESPACE}/' ${DEPLOY_YML_FILE}"
              sh "kubectl create -f ${DEPLOY_YML_FILE}"
              sh "kubectl expose deployment --namespace=${MST_NAMESPACE} ${MST_SVC_PREFIX}-${MS_NAME} --type=LoadBalancer --port=${PORT_LB} --target-port=${TARGET_PORT_LB}"
              sh "kubectl get pods --namespace=${MST_NAMESPACE}"
              sh "kubectl get deployment --namespace=${MST_NAMESPACE}"
              sh "kubectl get svc --namespace=${MST_NAMESPACE}"
              echo "Running the Service and Getting URL: ..."
              SVC_EXTERNAL_IP = sh(script: "kubectl get svc --namespace=${MST_NAMESPACE} | grep \"${MST_SVC_PREFIX}-${MS_NAME}\" | awk '{print \$4}'", returnStdout: true).trim()
              SVC_INTERNAL_IP = sh(script: "kubectl get svc --namespace=${MST_NAMESPACE} | grep \"${MST_SVC_PREFIX}-${MS_NAME}\" | awk '{print \$3}'", returnStdout: true).trim()
              SVC_URL = sh(script: "kubectl get svc --namespace=${MST_NAMESPACE} | grep \"${MST_SVC_PREFIX}-${MS_NAME}\" | awk '{print \$4}'", returnStdout: true).trim()
              echo "Your External IP is: ${SVC_EXTERNAL_IP}"
              echo "Yout Internal IP is: ${SVC_INTERNAL_IP}"
              echo "Running the Service and Getting URL: ...wait for 75 seconds!"
              sh 'sleep 75'
              echo "This is your URL: http://${SVC_URL}:${PORT_LB}/${API_PATH}"
            }
          }
        }
      }
    }//try
    catch (e) {
      currentBuild.result = 'FAILURE'
      throw e
      } finally {
      notifySlack(currentBuild.result)
      slackSend color: 'good', message: "${SERVICE_NAME}-${version} has been deployed to Pre-Live environment"
      slackSend color: 'good', message: "This is your URL: http://${SVC_URL}:${PORT_LB}/${API_PATH}"
    }
    }
    else{
        echo "This is other branch..(${env.GIT_BRANCH})"
        echo "------------------------------------------------------------------------"
        echo "Pipeline will not proccess this branch!"
    }
  }
}
